import feedparser

def slashdot() -> List[str]:
    d = feedparser.parse('http://rss.slashdot.org/Slashdot/slashdotMain')
    parsed = []
    for entry in d['entries']:
        entry_title = entry['title']
        entry_id = entry['links'][0]['href'].split('?utm_source')[0]
        parsed.append({'id': entry_id, 'title': entry_title})
    return(parsed)

print(slashdot())