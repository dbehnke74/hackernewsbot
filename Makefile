
.PHONY: setup run

all: setup run dockerbuild dockerrun

setup:
	python3 -m venv pyenv
	source pyenv/bin/activate && pip install -r requirements.txt

run:
	source pyenv/bin/activate && python3 hackernewsbot.py

dockerbuild:
	docker build -t hackernewsbot .

dockerrun:
	docker run --name hackernewsbot -d --restart always -v "$$PWD/hackernews.db:/usr/src/app/hackernews.db" hackernewsbot

dockerstop:
	- docker stop hackernewsbot
	- docker rm hackernewsbot

clean:
	- rm -r -f __pycache__
	- rm -r -f pyenv

